package dev.dontblameme.sgc.main

import dev.dontblameme.kutilsapi.updates.UpdateManager
import dev.dontblameme.sgc.utils.FileReader
import org.bukkit.plugin.java.JavaPlugin

/**
 * Creator: DontBlameMe69
 * Date: Mar 11, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

class SimpleGUICreator: JavaPlugin() {

    private val updateUrl = "https://codeberg.org/DontBlameMe69/SimpleGUICreator/releases/latest"

    override fun onEnable() {
        // Check updates
        if(UpdateManager(updateUrl, this).checkNewVersion())
            logger.warning("There is a new version available! Download it at $updateUrl")

        if(!this.dataFolder.exists()) this.dataFolder.mkdirs()

        FileReader(this)

        logger.info("Successfully loaded.")
    }

    override fun onDisable() {
        logger.info("Successfully unloaded.")
    }

}