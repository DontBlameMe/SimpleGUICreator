package dev.dontblameme.sgc.utils

import dev.dontblameme.kutilsapi.inventory.InventoryBuilder

/**
 * Creator: DontBlameMe69
 * Date: Mar 11, 2023
 * License: GPL-3.0-only or GPL-3.0-or-later
 */

data class CommandGUIs(val dir: String, val builder: HashMap<String, InventoryBuilder>) {}